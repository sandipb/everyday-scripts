====================
Every day scripts
====================

Collection of scripts that I use on my desktops and servers.

Included are:

1. ``laptop-shared-wifi``: Share network connection on your Linux host by
   configuring it as a wifi access point.
2. ``photo_label.py``: Use the file name in photos to label a folder full of them suitable for
   display on our PixStar photo frame.
